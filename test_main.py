import pytest
from main import max_profit

params = [
(-1, [0]),  
( 1, [0,1]),
(-1, [1,0]),  
( 2, [1,2,3]),   
(-1, [3,2,1]),  
( 1, [3,1,2]),   
( 2, [3,2,4]),   
( 2, [2,3,4]),   
( 2, [2,3,2,4]),   
( 1, [2,3,3,3]),   
(-1, [3,3,3,3]),  
(16, [44,30,24,32,35,30,40,38,15]),
(-1, [10,9,8,2]),
]  

@pytest.mark.parametrize("expected,price_points", params)
def test_max_profit(price_points, expected):
    assert expected == max_profit(price_points)

