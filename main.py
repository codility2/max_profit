def max_profit(price_points):
    max_profit = 0
    cnt = len(price_points)
    for i in range(cnt-1):
        for j in range(i, cnt):
            profit = price_points[j] - price_points[i]
            max_profit = max(max_profit, profit) 
    return max_profit or -1
